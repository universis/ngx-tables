import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedAggregatorListComponent } from './advanced-aggregator-list.component';

describe('AdvancedAggregatorListComponent', () => {
  let component: AdvancedAggregatorListComponent;
  let fixture: ComponentFixture<AdvancedAggregatorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedAggregatorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedAggregatorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
