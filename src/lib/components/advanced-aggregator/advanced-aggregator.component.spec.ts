import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedAggregatorComponent } from './advanced-aggregator.component';

describe('AdvancedAggregatorComponent', () => {
  let component: AdvancedAggregatorComponent;
  let fixture: ComponentFixture<AdvancedAggregatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedAggregatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedAggregatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
