import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { AdvancedTableComponent } from './components/advanced-table/advanced-table.component';
import { BehaviorSubject, Observable } from 'rxjs';
import { skip } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActivatedTableService {
  private _activeTable?: AdvancedTableComponent;
  /**
   * Get the last table that was set as active
   * Note: Emitted value may be undefined (e.g. reset on route changes)
   */
  public readonly lastActiveTable:
    BehaviorSubject<AdvancedTableComponent | undefined> = new BehaviorSubject<AdvancedTableComponent | undefined>(undefined);
  /**
   * Omit the last emitted value to observe only new active table changes
   * Note: Emitted value may be undefined (e.g. reset on route changes)
   */
  public readonly nextActiveTable:
    Observable<AdvancedTableComponent | undefined> = this.lastActiveTable.pipe(skip(1));

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) {
    _router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (this._activeTable) {
          this.activeTable = undefined;
        }
      }
    });
  }

  get activeTable(): AdvancedTableComponent | undefined {
    return this._activeTable;
  }

  set activeTable(tableComponent: AdvancedTableComponent | undefined) {
    this._activeTable = tableComponent;
    // provide change to observers
    this.lastActiveTable.next(this._activeTable);
  }
}
