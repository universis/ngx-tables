import { Action } from '@ngrx/store';

export enum TableActionTypes {
  LoadFilter = '[Table] Load Filter',
  AppendFilter = '[Table] Append Filter',
  AppendState = '[Table] Append State'
}

export class LoadFilter implements Action {
  readonly type = TableActionTypes.LoadFilter;
  constructor(public readonly id: string) {}
}

export class AppendFilter implements Action {
  readonly type = TableActionTypes.AppendFilter;
  constructor(public readonly id: string, public readonly filter: any) {}
}

export class AppendState implements Action {
  readonly type = TableActionTypes.AppendState;
  constructor(public readonly id: string, public readonly data: any) {}
}

export type TableActions = LoadFilter | AppendFilter | AppendState;
