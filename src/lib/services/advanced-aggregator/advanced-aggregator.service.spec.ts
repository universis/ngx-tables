import { TestBed } from '@angular/core/testing';

import { AdvancedAggregatorService } from './advanced-aggregator.service';

describe('AdvancedAggregatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvancedAggregatorService = TestBed.get(AdvancedAggregatorService);
    expect(service).toBeTruthy();
  });
});
